package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

func main() {
	r := gin.Default()
	m := melody.New()
	mAll := melody.New()

	r.GET("/", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "index.html")
	})

	r.GET("/channel/:name", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "chan.html")
	})

	r.GET("/channel/:name/ws", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})

	m.HandleMessage(func(s *melody.Session, msg []byte) {
		fmt.Println(msg)
		m.BroadcastFilter(msg, func(q *melody.Session) bool {
			fmt.Println(q.Request.URL.Path, s.Request.URL.Path)
			return q.Request.URL.Path == s.Request.URL.Path
		})
	})

	r.GET("/ws", func(c *gin.Context) {
		mAll.HandleRequest(c.Writer, c.Request)
	})

	mAll.HandleMessage(func(s *melody.Session, msg []byte) {
		mAll.Broadcast(msg)
	})

	m.HandleConnect(func(s *melody.Session) {
		fmt.Println("Connected: " + s.Request.Host)
	})

	m.HandleDisconnect(func(s *melody.Session) {
		fmt.Println("Disconnected" + s.Request.Host)
	})

	r.Run(":5000")
}
